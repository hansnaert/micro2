/******************************************************************************/
/* Files to Include                                                           */
/******************************************************************************/

#ifdef __XC32
#include <xc.h>          /* Defines special funciton registers, CP0 regs  */
#endif

#include <plib.h>            /* Include to use PIC32 peripheral libraries     */
#include <stdint.h>          /* For uint32_t definition                       */
#include <stdbool.h>         /* For true/false definition                     */
#include "user.h"            /* variables/params used by user.c               */

#include "system.h"

/******************************************************************************/
/* User Functions                                                             */
/******************************************************************************/

/* TODO Initialize User Ports/Peripherals/Project here */

void InitApp(void) {
    /* Setup analog functionality and port direction */
    ANSELAbits.ANSA0 = 0;
    ANSELAbits.ANSA1 = 0;

    TRISAbits.TRISA0 = 0;
    TRISAbits.TRISA1 = 0;

    LATAbits.LATA0 = 0;
    LATAbits.LATA1 = 0;

    ANSELBbits.ANSB3 = 0;
    ANSELBbits.ANSB13 = 0;
    TRISBbits.TRISB13 = 1;

    PPSUnLock;
    PPSOutput(1, RPB3, U1TX);
    PPSInput(3, U1RX, RPB13);
    PPSLock;

    UARTConfigure(UART1, UART_ENABLE_PINS_TX_RX_ONLY);
    UARTSetFifoMode(UART1, UART_INTERRUPT_ON_TX_NOT_FULL | UART_INTERRUPT_ON_RX_NOT_EMPTY);
    UARTSetLineControl(UART1, UART_DATA_SIZE_8_BITS | UART_PARITY_NONE | UART_STOP_BITS_1);
    UARTSetDataRate(UART1, SYS_FREQ, 9600);
    UARTEnable(UART1, UART_ENABLE_FLAGS(UART_PERIPHERAL | UART_RX | UART_TX));
   
}
